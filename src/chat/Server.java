package chat;

import java.io.*;
import java.net.*;

public class Server
{
    public static void main(String[] args)
    {
        try
        {
            ServerSocket serverSocket = new ServerSocket(1201);
            Socket socket = serverSocket.accept();

            DataInputStream din = new DataInputStream(socket.getInputStream());
            DataOutputStream dout = new DataOutputStream(socket.getOutputStream());

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

            String msgin = "";
            String msgout = "";

            while (!msgin.equals("end"))
            {
                msgin = din.readUTF();
                System.out.println(msgin);
                msgout = bufferedReader.readLine();
                dout.writeUTF("Server: " + msgout);
                dout.flush();
            }

            socket.close();

        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}
